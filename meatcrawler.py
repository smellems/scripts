# -*- coding: utf-8 -*-
import scrapy

class MeatcrawlerSpider(scrapy.Spider):
  name = 'meatcrawler'

  def start_requests(self):
    urls = [
      "http://www.rheintal.ca/boutique/fr/Porc-de-paturage-c1.html?limit=100",
      "http://www.rheintal.ca/boutique/fr/Veau-sous-la-mere-c2.html?limit=100",
      "http://www.rheintal.ca/boutique/fr/Veau-sous-la-mere-c2.html?limit=100",
      "http://www.rheintal.ca/boutique/fr/Poulet-picoreur-c8.html?limit=100",
      "http://www.rheintal.ca/boutique/fr/Charcuteries-authentiques-c5.html?limit=100",
      "http://www.rheintal.ca/boutique/fr/Charcuterie-sechee-c4.html?limit=100",
      "http://www.rheintal.ca/boutique/fr/Produits-derives-c6.html?limit=100",
    ]
    for url in urls:
      yield scrapy.Request(url=url, callback=self.parsec)

  def parsec(self, response):
    for product in response.css("div.box-product div.boxgrid-bottom"):
      urlp = product.css("div.name a::attr(href)").extract_first();
      yield scrapy.Request(url=urlp, callback=self.parsep);
          
  def parsep(self, response):
    yield {
      'name': response.css("div.heading-product h1::text").extract_first(),
      'category': response.css("div.breadcrumb a::text").extract()[1],
      'code': response.css("div.description::text").extract()[1].strip(),
      'pricekg': response.css("div.description::text").extract()[4].replace("$", "").strip(),
      'price': response.css("div.price::text").extract_first().replace("Prix :", "").replace("$", "").replace(".", "").replace(",", ".").strip(),
      'approxkg': response.css("div.right::text").extract()[8].replace("\xa0kg.", "").strip(),
    }